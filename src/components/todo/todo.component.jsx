import "./todo.component.css";
import axios from "axios";
import { Component } from "react";

class Todo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
      filteredTodos: [],
      error: null,
      search: "",
    };
  }

  async getTodos() {
    axios
      .get("https://jsonplaceholder.typicode.com/todos/")
      .then(({ data }) => {
        this.setState({
          todos: data.slice(0, 10),
          filteredTodos: data.slice(0, 10),
        });
      })
      .catch((error) => this.setState({ error: "Failed to retrieve Todos!" }));
  }

  componentDidMount() {
    this.getTodos();
  }

  handleSelectChange = (todo, event) => {
    todo.completed = event.target.checked;
    this.setState({ todos: [...this.state.todos, todo] });
  };

  filterTodos = (event) => {
    this.setState({ search: event.target.value });
    setTimeout(() => {
      this.setState({
        filteredTodos: this.state.todos.filter((todo) => {
          return (
            todo.title.includes(this.state.search) ||
            this.state.search.trim() === ""
          );
        }),
      });
    }, 200);
  };

  render() {
    return (
      <div className="todo">
        <h1>Todo</h1>

        <div className="search">
          <input
            type="text"
            placeholder="Search..."
            value={this.state.search}
            onChange={(event) => this.filterTodos(event)}
          />
        </div>

        <ul>
          {this.state.filteredTodos.map((todo) => {
            return (
              <li key={todo.id}>
                <span>{todo.title}</span>
                <input
                  type="checkbox"
                  checked={todo.completed}
                  onChange={(event) => this.handleSelectChange(todo, event)}
                />
              </li>
            );
          })}
        </ul>

        <span>{this.state.error}</span>
      </div>
    );
  }
}

export default Todo;
