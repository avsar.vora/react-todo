import './App.css';
import Todo from './components/todo/todo.component';

function App() {
  return (
    <div className="App">
      <Todo />
    </div>
  );
}

export default App;
